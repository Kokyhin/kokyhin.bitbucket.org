$( document ).ready(function() {
  $('.slider-goods').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    responsive:{
      0:{
        items:1
      }
    }
  });

  $('.slider-arrow').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
    responsive:{
      0:{
        items:1
      }
    }
  });

  $('.main-slider').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    dots: false,
    navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
    responsive:{
      0:{
        items:1
      }
    }
  });

  $('.card-slider').owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    dots: false,
    // navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
    responsive:{
      0:{
        items:1
      }
    }
  });

  $('.slider-similar-goods').owlCarousel({
    loop: true,
    margin: 15,
    nav: true,
    dots: false,
    navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
    responsive:{
      0:{
        items:1
      },
      900:{
        items:2
      },
      1500: {
        items: 4
      }
    }
  });
  var owl = $(".card-slider").owlCarousel();
  var items = $('.nav-item');
  $(document).on('click', '.nav-item', function(ev){
    $('.nav-item').each(function(i, el) {
      $(el).removeClass('active');
    }, this);
    ev.currentTarget.classList.add("active");
    var index = +ev.currentTarget.dataset.index;
    owl.trigger('to.owl.carousel', [index, 200])
  });

});
